﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoSorter
{
    public partial class MainForm : Form
    {
        private string[] FromFiles { get; set; }
        private string ToPath { get; set; }

        private long _total;
        private long _current;
        public double Progress => _total == 0 ? 0 : (double)_current / _total * 100;

        public MainForm()
        {
            InitializeComponent();
        }

        private void FromUrl_Btn_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result != DialogResult.OK || string.IsNullOrWhiteSpace(fbd.SelectedPath)) return;

                FromUrl_Label.Text = fbd.SelectedPath;

                FromFiles = Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories);

                FilesCount_Label.Text = FromFiles.Length.ToString();
            }
        }

        private void ToUrl_Btn_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result != DialogResult.OK || string.IsNullOrWhiteSpace(fbd.SelectedPath)) return;

                ToPath = fbd.SelectedPath;

                ToUrl_Label.Text = ToPath;
            }
        }

        private async void Scan_Btn_Click(object sender, EventArgs e)
        {
            if (FromFiles.Length <= 0 || ToPath == null) return;

            _total = FromFiles.LongCount();
            _current = 0;

            FromUrl_Btn.Enabled = false;
            ToUrl_Btn.Enabled = false;
            Scan_Btn.Enabled = false;

            await Task.Run(() => Parallel.ForEach(FromFiles, file =>
            {
                try
                {
                    var dates = new List<DateTime>
                    {
                        File.GetCreationTime(file),
                        File.GetLastWriteTime(file),
                        File.GetLastAccessTime(file)
                    };

                    var minDate = dates.Min();

                    // ReSharper disable StringLiteralTypo
                    var creationYear = minDate.Year.ToString();
                    var creationMonth = minDate.ToString("MMMM", CultureInfo.CreateSpecificCulture("pl"));
                    // ReSharper restore StringLiteralTypo

                    var yearPath = Path.Combine(ToPath, creationYear);
                    if (!Directory.Exists(yearPath))
                    {
                        Directory.CreateDirectory(yearPath);
                    }

                    var monthPath = Path.Combine(yearPath, creationMonth);
                    if (!Directory.Exists(monthPath))
                    {
                        Directory.CreateDirectory(monthPath);
                    }

                    var destFileName = Path.Combine(monthPath, Path.GetFileName(file));

                    var i = 0;
                    while (File.Exists(destFileName))
                    {
                        var fileName = Path.GetFileNameWithoutExtension(file);
                        var fileExtension = Path.GetExtension(file);

                        destFileName = Path.Combine(monthPath, $"{fileName + i}{fileExtension}");
                        i++;
                    }

                    File.Copy(file, destFileName);
                }
                catch (Exception)
                {
                    //skip
                }
                finally
                {
                    Interlocked.Increment(ref _current);
                    UpdateProgress();
                }

            }));
        }

        private void UpdateProgress()
        {
            Invoke(new Action(() => ProgressBar.Value = (int)Progress));
            Invoke(new Action(() =>
            {
                if ((int)Math.Floor(Progress) != 100) return;

                FromUrl_Btn.Enabled = true;
                ToUrl_Btn.Enabled = true;
                Scan_Btn.Enabled = true;
            }));
        }
    }
}
