﻿namespace PhotoSorter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.FromUrl_Btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ToUrl_Btn = new System.Windows.Forms.Button();
            this.FromUrl_Label = new System.Windows.Forms.Label();
            this.ToUrl_Label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.FilesCount_Label = new System.Windows.Forms.Label();
            this.Scan_Btn = new System.Windows.Forms.Button();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // FromUrl_Btn
            // 
            this.FromUrl_Btn.Location = new System.Drawing.Point(12, 12);
            this.FromUrl_Btn.Name = "FromUrl_Btn";
            this.FromUrl_Btn.Size = new System.Drawing.Size(75, 23);
            this.FromUrl_Btn.TabIndex = 1;
            this.FromUrl_Btn.Text = "Wybierz";
            this.FromUrl_Btn.UseVisualStyleBackColor = true;
            this.FromUrl_Btn.Click += new System.EventHandler(this.FromUrl_Btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Forder wejściowy:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Folder wyjściowy:";
            // 
            // ToUrl_Btn
            // 
            this.ToUrl_Btn.Location = new System.Drawing.Point(12, 41);
            this.ToUrl_Btn.Name = "ToUrl_Btn";
            this.ToUrl_Btn.Size = new System.Drawing.Size(75, 23);
            this.ToUrl_Btn.TabIndex = 4;
            this.ToUrl_Btn.Text = "Wybierz";
            this.ToUrl_Btn.UseVisualStyleBackColor = true;
            this.ToUrl_Btn.Click += new System.EventHandler(this.ToUrl_Btn_Click);
            // 
            // FromUrl_Label
            // 
            this.FromUrl_Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FromUrl_Label.AutoSize = true;
            this.FromUrl_Label.Location = new System.Drawing.Point(190, 17);
            this.FromUrl_Label.Name = "FromUrl_Label";
            this.FromUrl_Label.Size = new System.Drawing.Size(16, 13);
            this.FromUrl_Label.TabIndex = 5;
            this.FromUrl_Label.Text = "...";
            // 
            // ToUrl_Label
            // 
            this.ToUrl_Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ToUrl_Label.AutoSize = true;
            this.ToUrl_Label.Location = new System.Drawing.Point(190, 46);
            this.ToUrl_Label.Name = "ToUrl_Label";
            this.ToUrl_Label.Size = new System.Drawing.Size(16, 13);
            this.ToUrl_Label.TabIndex = 6;
            this.ToUrl_Label.Text = "...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Liczba plików do posortowania:";
            // 
            // FilesCount_Label
            // 
            this.FilesCount_Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FilesCount_Label.AutoSize = true;
            this.FilesCount_Label.Location = new System.Drawing.Point(190, 75);
            this.FilesCount_Label.Name = "FilesCount_Label";
            this.FilesCount_Label.Size = new System.Drawing.Size(16, 13);
            this.FilesCount_Label.TabIndex = 8;
            this.FilesCount_Label.Text = "...";
            // 
            // Scan_Btn
            // 
            this.Scan_Btn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Scan_Btn.Location = new System.Drawing.Point(12, 130);
            this.Scan_Btn.Name = "Scan_Btn";
            this.Scan_Btn.Size = new System.Drawing.Size(534, 23);
            this.Scan_Btn.TabIndex = 13;
            this.Scan_Btn.Text = "Sortuj";
            this.Scan_Btn.UseVisualStyleBackColor = true;
            this.Scan_Btn.Click += new System.EventHandler(this.Scan_Btn_Click);
            // 
            // ProgressBar
            // 
            this.ProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressBar.Location = new System.Drawing.Point(12, 101);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(534, 23);
            this.ProgressBar.TabIndex = 14;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 165);
            this.Controls.Add(this.ProgressBar);
            this.Controls.Add(this.Scan_Btn);
            this.Controls.Add(this.FilesCount_Label);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ToUrl_Label);
            this.Controls.Add(this.FromUrl_Label);
            this.Controls.Add(this.ToUrl_Btn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FromUrl_Btn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Sortowanie zdjęć na lata i miesiące";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button FromUrl_Btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ToUrl_Btn;
        private System.Windows.Forms.Label FromUrl_Label;
        private System.Windows.Forms.Label ToUrl_Label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label FilesCount_Label;
        private System.Windows.Forms.Button Scan_Btn;
        private System.Windows.Forms.ProgressBar ProgressBar;
    }
}

